# Musicord2

## A music bot for discord

<img alt="Musicord Logo" src="./assets/icon.jpg" width=250>

## Usage Info

### To run your own instance

Docker container is avaliable at [illemonati7/musicord](https://hub.docker.com/repository/docker/illemonati7/musicord)

Set the environment variable MUSICORD_DISCORD_TOKEN to your discord token

### To add the public instance of the bot to your server

[Add to your server](https://discord.com/api/oauth2/authorize?client_id=960326632864874537&permissions=535326883137&scope=applications.commands%20bot)

## Proposed Features

### Stage 1

-   Support for YouTube video by id or url
-   Support for queue system with basic enqueuing functionally
-   Play, skip, and stop

### Stage 2

-   Support YouTube playlist from url
-   Loop, pause, resume

### Stage 3

-   Web interface to manage playback and queue
-   able to control bot through web interface

### Stage 4

-   YouTube search functionality
-   Enhance queue system to be able to insert and remove tracks at different location
-   Enhance queue system to be able to move tracks in different locations

Any and all suggestions as well as contributions are welcome.
