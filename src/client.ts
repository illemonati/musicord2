import {
    Client,
    Collection,
    Events,
    GatewayIntentBits,
    REST,
    Routes,
} from "discord.js";
import { Musicord2Command } from "./command";
import { BartCommand } from "./commands/bart";
import { MusicPlayer } from "./music-player/music-player";

export class Musicord2Client {
    client: Client;
    clientCommands: Collection<string, Musicord2Command>;
    musicPlayer: MusicPlayer;

    constructor() {
        this.client = new Client({
            intents: [
                GatewayIntentBits.Guilds,
                GatewayIntentBits.GuildVoiceStates,
            ],
        });
        this.clientCommands = new Collection();
        this.musicPlayer = new MusicPlayer(this);
    }

    async initialize(token: string) {
        this.client.login(token);

        this.client = await new Promise((r) =>
            this.client.once(Events.ClientReady, (client) => r(client))
        );

        if (!this.client.isReady()) {
            throw new Error("Client Not Ready");
        }

        console.log(`Logged in as ${this.client.user?.tag}`);

        await this.loadCommands();
        console.log(`Loaded commands`);

        try {
            await this.registerCommands();

            this.startHandleCommands();
            console.log(`Started handle commands`);
        } catch (e) {
            console.error(e);
        }
    }

    async loadCommands() {
        const commands: Musicord2Command[] = [
            ...this.musicPlayer.loadCommands(),
            ...[BartCommand].map((command) => new command(this)),
        ];
        for (const command of commands) {
            if (command.name) {
                await command.initialize();
                await command.finalize();
                this.clientCommands.set(command.name, command);
            }
        }
    }

    async registerCommands() {
        if (!this.client.application) {
            throw new Error("Client does not have application");
        }

        try {
            const data: unknown = await this.client.rest.put(
                Routes.applicationCommands(this.client.application.id),
                {
                    body: Array.from(this.clientCommands.values()).map(
                        (command) => command.data
                    ),
                }
            );

            console.log(
                `Successfully loaded ${
                    (data as unknown as any).length || "unknown"
                } application (/) commands.`
            );
        } catch (e) {
            throw Error("Unable to register commands");
        }
    }

    startHandleCommands() {
        this.client.on(Events.InteractionCreate, async (interaction) => {
            if (!interaction.isChatInputCommand()) return;

            const command = this.clientCommands.get(interaction.commandName);

            try {
                await command?.execute(interaction);
            } catch (e) {
                if (interaction.replied || interaction.deferred) {
                    await interaction.followUp({
                        content:
                            "There was an error while executing this command!",
                        ephemeral: true,
                    });
                } else {
                    await interaction.reply({
                        content:
                            "There was an error while executing this command!",
                        ephemeral: true,
                    });
                }
            }
        });
    }
}
