import { Client, Events, GatewayIntentBits } from "discord.js";
import * as dotenv from "dotenv";
import { Musicord2Client } from "./client";

const main = async () => {
    dotenv.config();

    if (!process.env.MUSICORD2_DISCORD_TOKEN) {
        throw new Error("Token Not Found");
    }
    const client = new Musicord2Client();
    client.initialize(process.env.MUSICORD2_DISCORD_TOKEN);
};

main().then();
