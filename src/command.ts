import {
    SlashCommandBuilder,
    Interaction,
    ChatInputCommandInteraction,
} from "discord.js";
import { Musicord2Client } from "./client";

export class Musicord2Command {
    name?: string;
    description?: string;
    data: SlashCommandBuilder;
    musicord2: Musicord2Client;

    constructor(musicord2: Musicord2Client) {
        this.data = new SlashCommandBuilder();
        this.musicord2 = musicord2;
    }

    async initialize() {}

    async finalize() {
        this.name && this.data.setName(this.name);
        this.description && this.data.setDescription(this.description);
    }

    async execute(interaction: ChatInputCommandInteraction) {}
}
