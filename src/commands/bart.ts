import {
    SlashCommandBuilder,
    Interaction,
    ChatInputCommandInteraction,
} from "discord.js";
import { Musicord2Command } from "../command";

export class BartCommand extends Musicord2Command {
    name = "bart";
    description = "Tests out :bart:";

    async execute(interaction: ChatInputCommandInteraction) {
        if (interaction.isRepliable()) {
            interaction.reply(":bart:");
        }
    }
}
