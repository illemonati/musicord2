import { Musicord2Client } from "../client";
import { Musicord2Command } from "../command";
import { MusicPlayer } from "./music-player";

export class MusicPlayerCommand extends Musicord2Command {
    player: MusicPlayer;

    constructor(player: MusicPlayer, musicord2: Musicord2Client) {
        super(musicord2);
        this.player = player;
    }
}
