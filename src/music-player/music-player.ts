import { Collection, Guild, VoiceBasedChannel } from "discord.js";
import { Musicord2Client } from "../client";
import { PlayCommand } from "./commands/play";
import { MusicPlayerCommand } from "./music-player-command";
import { MusicPlayerQueue } from "./music-player-queue";
import { joinVoiceChannel } from "@discordjs/voice";
import { Song } from "./song";
import { PauseCommand } from "./commands/pause";
import { ResumeCommand } from "./commands/resume";
import { StopCommand } from "./commands/stop";
import { SkipCommand } from "./commands/skip";
import { ListQueueCommand } from "./commands/list";

type GuildID = string;
export class MusicPlayer {
    queues: Collection<GuildID, MusicPlayerQueue>;
    musicord2: Musicord2Client;

    constructor(musicord2: Musicord2Client) {
        this.queues = new Collection();
        this.musicord2 = musicord2;
    }

    loadCommands() {
        return [
            PlayCommand,
            PauseCommand,
            ResumeCommand,
            StopCommand,
            SkipCommand,
            ListQueueCommand,
        ].map((command) => new command(this, this.musicord2));
    }

    joinChannel(channel: VoiceBasedChannel) {
        joinVoiceChannel({
            channelId: channel.id,
            guildId: channel.guildId,
            adapterCreator: channel.guild.voiceAdapterCreator,
        });
    }

    async enqueueSong(
        guildId: string,
        url: string,
        requesterTag?: string,
        requesterAvatarURL?: string
    ) {
        let queue = this.queues.get(guildId);

        if (!queue) {
            queue = new MusicPlayerQueue(guildId);
            this.queues.set(guildId, queue);
        }

        return await queue.enqueueSong(url, requesterTag, requesterAvatarURL);
    }

    getQueue(guildId: string) {
        const queue = this.queues.get(guildId);
        if (!queue) {
            throw new Error("Cannot play without queue");
        }
        return queue;
    }

    play(guildId: string) {
        this.getQueue(guildId).play();
    }

    pause(guildId: string) {
        this.getQueue(guildId).pause();
    }

    skip(guildId: string) {
        this.getQueue(guildId).skip();
    }

    stop(guildId: string) {
        this.getQueue(guildId).stop();
    }
}
