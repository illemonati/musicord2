import {
    AudioPlayer,
    AudioPlayerStatus,
    createAudioPlayer,
    createAudioResource,
    getVoiceConnection,
} from "@discordjs/voice";
import { Song } from "./song";
import { EmbedBuilder } from "discord.js";
import { DISCORD_BACKGROUND_COLOR, DISCORD_EMBED_COLOR } from "../consts";

export class MusicPlayerQueue {
    queue: Song[] = [];
    playing: boolean = false;
    guildId: string;
    audioPlayer: AudioPlayer;

    constructor(guildId: string) {
        this.guildId = guildId;
        this.audioPlayer = createAudioPlayer();
        this.audioPlayer.on(AudioPlayerStatus.Idle, () => {
            this.handleSongEnd();
        });
    }

    async enqueueSong(
        url: string,
        requesterTag?: string,
        requesterAvatarURL?: string
    ) {
        const song = await Song.fromURL(url);

        song.requesterTag = requesterTag;
        song.requesterAvatarURL = requesterAvatarURL;

        this.queue.push(song);
        return song;
    }

    isEmpty() {
        return this.queue.length < 1;
    }

    handleSongEnd() {
        this.queue.shift();
        this.audioPlayer.stop();
        if (this.isEmpty()) {
            this.stop();
        } else {
            this.play(true);
        }
    }

    play(skip?: boolean) {
        if (this.isEmpty()) {
            throw new Error("Empty Queue");
        }

        const currentSong = this.queue[0];
        const connection = getVoiceConnection(this.guildId);

        if (!connection) {
            throw new Error("No audio channel connected");
        }

        connection.subscribe(this.audioPlayer);

        if (!skip && this.audioPlayer.checkPlayable()) {
            this.audioPlayer.unpause();
            return;
        }

        const stream = currentSong.createReadSteam();

        const resource = createAudioResource(stream);

        this.audioPlayer.play(resource);
    }

    pause() {
        this.audioPlayer.pause();
    }

    skip() {
        this.audioPlayer.stop();
        this.handleSongEnd();
    }

    stop() {
        this.audioPlayer.stop();
        this.queue = [];
        const connection = getVoiceConnection(this.guildId);

        if (!connection) {
            throw new Error("No audio channel connected");
        }

        connection.disconnect();
    }

    toString() {
        let res = "";
        for (let i = 0; i < this.queue.length; i++) {
            res += `${i.toString().padStart(3, "")}) ${this.queue[i].title}\n`;
        }
        return res;
    }

    createEmbed(requesterTag?: string, requesterAvatarURL?: string) {
        const embed = new EmbedBuilder();
        embed.setTitle("Song Queue");
        embed.setColor(DISCORD_EMBED_COLOR);
        if (requesterTag) {
            embed.setFooter({
                text: requesterTag,
                iconURL: requesterAvatarURL,
            });
        }
        embed.setDescription(this.toString());

        return embed;
    }
}
