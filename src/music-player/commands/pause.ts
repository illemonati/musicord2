import { ChatInputCommandInteraction, GuildMember } from "discord.js";
import { MusicPlayerCommand } from "../music-player-command";
import { getVoiceConnection } from "@discordjs/voice";

export class PauseCommand extends MusicPlayerCommand {
    name = "pause";
    description = "Pause the currently playing audio";

    async execute(interaction: ChatInputCommandInteraction) {
        if (!interaction.guildId) {
            await interaction.reply("Please only use this command in a server");
            return;
        }

        const guildVoiceChannel = getVoiceConnection(interaction.guildId);

        if (!guildVoiceChannel) {
            await interaction.reply("No voice channels connected");
            return;
        }

        this.player.pause(interaction.guildId);

        await interaction.reply("Paused");
    }
}
