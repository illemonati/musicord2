import { ChatInputCommandInteraction, GuildMember } from "discord.js";
import { MusicPlayerCommand } from "../music-player-command";
import { getVoiceConnection } from "@discordjs/voice";

export class ListQueueCommand extends MusicPlayerCommand {
    name = "list";
    description = "List the current queue";

    async execute(interaction: ChatInputCommandInteraction) {
        if (!interaction.guildId) {
            await interaction.reply("Please only use this command in a server");
            return;
        }

        const guildVoiceChannel = getVoiceConnection(interaction.guildId);

        if (!guildVoiceChannel) {
            await interaction.reply("No voice channels connected");
            return;
        }

        await interaction.reply({
            embeds: [
                this.player
                    .getQueue(interaction.guildId)
                    .createEmbed(
                        interaction.user.tag,
                        interaction.user.avatarURL() || undefined
                    ),
            ],
        });
    }
}
