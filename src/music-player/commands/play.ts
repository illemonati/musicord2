import {
    SlashCommandStringOption,
    ChatInputCommandInteraction,
    GuildMember,
} from "discord.js";
import { MusicPlayerCommand } from "../music-player-command";
import { getVoiceConnection } from "@discordjs/voice";

export class PlayCommand extends MusicPlayerCommand {
    name = "play";
    description = "plays a song by url";

    async initialize() {
        const urlOption = new SlashCommandStringOption()
            .setName("url")
            .setRequired(false)
            .setDescription("youtube link to play");

        this.data.addStringOption(urlOption);
    }

    async execute(interaction: ChatInputCommandInteraction) {
        const url = interaction.options.getString("url", false);

        if (!interaction.guildId) {
            await interaction.reply("Please only use this command in a server");
            return;
        }

        const member = interaction.member as GuildMember;

        if (!url) {
            const guildVoiceChannel = getVoiceConnection(interaction.guildId);

            if (!guildVoiceChannel) {
                await interaction.reply("No voice channels connected");
                return;
            }

            this.player.play(interaction.guildId);

            await interaction.reply("Resumed");
        } else {
            if (!member.voice?.channel) {
                await interaction.reply("Please be in a voice channel");
                return;
            }

            if (!member.voice.channel?.joinable) {
                await interaction.reply(
                    "I do not have permission to join this voice channel"
                );
                return;
            }

            this.player.joinChannel(member.voice.channel);

            const song = await this.player.enqueueSong(
                interaction.guildId,
                url,
                interaction.user.tag,
                interaction.user.avatarURL() || undefined
            );

            this.player.play(interaction.guildId);

            await interaction.reply({ embeds: [song.createEmbed()] });
        }
    }
}
