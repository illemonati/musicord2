import { EmbedBuilder } from "discord.js";
import { DateTime } from "luxon";
import ytdl = require("ytdl-core");
import { DISCORD_BACKGROUND_COLOR, DISCORD_EMBED_COLOR } from "../consts";

export class Song {
    title: string = "Unknown Song";
    url?: string;
    imageURL?: string;
    authorName?: string;
    authorAvatarURL?: string;
    duration: number = 0;
    position: number = 0;
    requesterTag?: string;
    requesterAvatarURL?: string;

    static async fromURL(url: string): Promise<Song> {
        const info = await ytdl.getBasicInfo(url);
        const song = new Song();
        song.title = info.videoDetails.title;
        song.url = url;
        song.duration = parseInt(info.videoDetails.lengthSeconds);
        song.position = 0;
        song.imageURL = info.videoDetails.thumbnails[0].url;
        song.authorName = info.videoDetails.author.name;
        if (info.videoDetails.author.thumbnails) {
            song.authorAvatarURL = info.videoDetails.author.thumbnails.reduce(
                (a, b) => (a.height > b.height ? a : b)
            ).url;
        }
        if (info.videoDetails.thumbnails) {
            song.imageURL = info.videoDetails.thumbnails.reduce((a, b) =>
                a.height > b.height ? a : b
            ).url;
        }
        return song;
    }

    createReadSteam() {
        if (!this.url) {
            throw new Error("No URL");
        }
        return ytdl(this.url, {
            filter: "audioonly",
            quality: "highestaudio",
            highWaterMark: 1 << 25,
        });
    }

    createEmbed() {
        const embed = new EmbedBuilder();
        embed.setTitle(this.title);
        embed.setColor(DISCORD_EMBED_COLOR);
        embed.setAuthor({
            name: this.authorName || "unknown",
            iconURL: this.authorAvatarURL,
        });

        this.url && embed.setURL(this.url);

        // embed.setDescription(
        //     DateTime.fromSeconds(this.duration).toFormat("mm:ss")
        // );

        if (this.imageURL) {
            embed.setImage(this.imageURL);
        }
        embed.setTimestamp(new Date());
        if (this.requesterTag) {
            embed.setFooter({
                text: this.requesterTag,
                iconURL: this.requesterAvatarURL,
            });
        }
        return embed;
    }
}
